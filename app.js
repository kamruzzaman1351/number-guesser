// Variable from UI
const gameUI = document.getElementById("game"),
      minNumUI = document.getElementById("min-num"),
      maxNumUI = document.getElementById("max-num"),
      guessInputUI = document.getElementById("guess-input"),
      guessSubmitUI = document.getElementById("guess-submit"),
      msgShowUI = document.getElementById("msg-show");


// Listen For a Submit or Click event
guessSubmitUI.addEventListener("click", numberGuess);
gameUI.addEventListener("mousedown", playAgain);
// This Game will require 
// Min-Number
// Max-Number
// Right Number
let minNumber = 1,
    maxNumber = 10,
    winingNum = Math.floor((Math.random() * maxNumber) + 1);;
    guessLeft = 3;

// Assign min and max Number to UI
minNumUI.textContent = minNumber;
maxNumUI.textContent = maxNumber;
//  Event Function
function numberGuess() {    
    const userGuess = parseInt(guessInputUI.value);
    if(isNaN(userGuess) || userGuess < minNumber || userGuess >maxNumber) {
        let msg = `Please give a number between ${minNumber} and ${maxNumber}`; 
        showMsg(msg, "red");
    }

    if (userGuess === winingNum) {
        // Game over - win
        msg = `${winingNum} is Correct, You Won!`;
        gameOver(true, msg)
        // showMsg(msg, "green");
        
    } else {
        guessLeft -= 1
        if(guessLeft === 0) {
            // Game Over - Lost
            msg = `Game Over! You Lost. The correct Number was ${winingNum}`
            gameOver(false,msg);            
        } else {
            msg = `Your guess ${userGuess} is not Correct. You have ${guessLeft} guess left`;
            showMsg(msg, "red");
            guessInputUI.value = "";
        }
               
    }

}

// Show Massage
function showMsg(msg, color) {
    msgShowUI.textContent = msg; 
    msgShowUI.style.color = color;   
}

// Game over win || lost
function gameOver(win, msg) {
    let color;
    win === true ? color="green" : color="red"; 
    guessInputUI.disabled = true;
    msgShowUI.style.color = color;
    guessInputUI.style.borderColor = color;
    showMsg(msg);
    // Play Again
    guessSubmitUI.value = "Play Again!";
    guessSubmitUI.style.color = "green";
    guessSubmitUI.className += "play-again";
    
}

// Play Again Event
function playAgain(e) {
    if(e.target.className === "play-again") {
        window.location.reload();
    }
}